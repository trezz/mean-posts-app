import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatCardModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PostCreateComponent } from './post-create.component';


describe('PostCreateComponent', () => {
  let component: PostCreateComponent;
  let fixture: ComponentFixture<PostCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostCreateComponent],
      imports: [MatCardModule, MatProgressSpinnerModule, MatInputModule, FormsModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  beforeEach(inject([PostCreateComponent], (form: PostCreateComponent) => {
    component = form;
  }));

  // it('should return a form', () => {
  //   const compiledDom = fixture.debugElement.nativeElement;
  //   compiledDom.querySelector('button').click();
  //   fixture.detectChanges();
  //   expect(fixture.debugElement.queryAll(By.css('.menu-item')).length).toEqual(2);
  // });
});
