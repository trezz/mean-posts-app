import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatProgressSpinnerModule, MatExpansionModule, MatPaginatorModule } from '@angular/material';


import { Post } from '../../../models/post.model';
import { PostListComponent } from './post-list.component';
import { AuthService } from 'src/app/services/auth.service';
import { PostsService } from '../../../services/post.service';

describe('PostListComponent', () => {
  let component: PostListComponent;
  let fixture: ComponentFixture<PostListComponent>;
  let posts: Post[] = [];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostListComponent],
      imports: [
        MatProgressSpinnerModule,
        MatExpansionModule,
        MatPaginatorModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([])]
    })

      .compileComponents().then(() => {
        fixture = TestBed.createComponent(PostListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create a post list component', () => {
    const authService: AuthService = TestBed.get(AuthService);
    const state = authService.getIsAuth();
    if (state) {
      const user = authService.getUserId();
      const postsPerPage = 2;
      const currentPage = 1;
      const postsService: PostsService = TestBed.get(PostsService);
      if (user) {
        expect(postsService.getPosts(postsPerPage, currentPage)).toEqual();
      } else {
        expect(postsService.getPosts(postsPerPage, currentPage)).toEqual();
      }
    } else {
      expect(authService.getIsAuth()).toBe(false);
    }
  });
});
