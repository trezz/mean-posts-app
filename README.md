# POSTS APP OVERVIEW

This app it is an mvp which allows user to share posts with title, images and text
It has :

* routes protection
* user restrictions
* ui restriction to unAuthenticated users
* angular material design
* file upload and backend handler with multer
* errors handler
* etc ...

users can view all posts but only has only access on modification or deletion to the posts he created

## NOTES

This is an mvp of a post app made with MEAN Stack (MongoDB, Express, Angular and NodeJS)

some of these files have many comments to help understand the code and the logic behind it.

It may seem misplaced or make the code difficult to read.

So to avoid that I empty all comments and I pushed nothing but the code in the last commit.
For those who want to read these comments please check the old commits so you will find these comments

Note for those who want to implement this code, please keep the same logic of comments in the development and at the end push the clean code if possible with some comments
thank you !!

## DEVELOPMENT SETUPS

* you need to have mongoDB installed in your machine or somewhere else on the cloud you have access on

## MeanCourse

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
