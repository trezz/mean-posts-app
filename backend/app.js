var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
const mongoose = require("mongoose");

const postsRoutes = require("./routes/posts");
const userRoutes = require("./routes/user");

//connect to mongodb

mongoose
  .connect('mongodb://localhost:27017/posts-mean-course', {
    useNewUrlParser: true
  })
  .then(() => console.log("Mongodb connected"))
  .catch(err => console.log(`erreur niyi ${err}`));


var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// giving access to the frontend to the images folder --- this alone won't work without path package
app.use("/images", express.static(("backend/images")));

app.use((req, res, next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    );
    next();
})


app.use("/api/posts", postsRoutes);
app.use("/api/user", userRoutes);


module.exports = app;